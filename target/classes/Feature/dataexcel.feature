Feature: Data driven using excel
  @Excel
  Scenario Outline:test login using excel
    Given user get details from <SheetName> for <TestCaseID>
    And user hits the URL
    When user enter cred details
    And  click submit button
    Then verify login successfully
    Examples:
      |SheetName|TestCaseID|
      |UserPassDetails|0|
      |UserPassDetails|1|
      |UserPassDetails|2|
      |UserPassDetails|3|