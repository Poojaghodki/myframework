Feature: test User login

  @testdata
  Scenario: test login
    Given user open browser and hit url
    When user enters username and password
    And hit submit button
    Then validate user landing on welcome page
    Examples:

  @Test
  Scenario Outline:test login
    Given user open browser and hit url
    When user enters <username> and <password>
    And hit submit button
    Then validate user landing on welcome page
    Examples:
      |username|password|
      |java@gmail.com|Selenium@123|
      |java@gmail.com|Selenium@123|

  @DataTablescenario
  Scenario: test login using dataTable
    Given valid user is opening Url
      |Url|
      |http://live.techpanda.org/index.php/customer/account/login/|
    When valid user enter valid UserName and valid Password
      |java@gmail.com|Selenium@123|
      |java@gmail1.com|Selenium@123|
      |java@gmail.com|Selenium@1234|
      |java@gmail.com|Selenium@123|
    And hit submit button
    Then validate user landing on welcome page
