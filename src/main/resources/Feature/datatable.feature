Feature: test login using data table

  Scenario:test login using data table
    Given user open browser and hit Url
      |Url|
      |http://live.techpanda.org/index.php/customer/account/login/|
    When user enters username and password
      |key|value|
      |java@gmail.com|Selenium@123|
      |java@gmail2.com|Selenium@123|
    And click on submit button
    Then validate user landing on welcome page successfully