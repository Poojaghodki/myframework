package pages;

import commonutils.PropertiesData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.time.Duration;

public class LoginData {

    public static void main(String[] args) throws IOException {

        System.setProperty("webdriver.chrome.driver","C:\\Users\\ghodk\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");

        //  WebDriverManager.chromedriver().mac().setup();
        //  WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();


        driver.get(PropertiesData.getData("login_url"));
        driver.manage().timeouts().getPageLoadTimeout();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        WebElement username=driver.findElement(By.id("email"));
        username.sendKeys(PropertiesData.getData("loginusername"));
        WebElement pass= driver.findElement(By.id("pass"));
        pass.sendKeys(PropertiesData.getData("loginpassword"));
        WebElement submit=driver.findElement(By.id("send2"));
        submit.submit();

    }
}

