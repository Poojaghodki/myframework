package pages;

import commonutils.PropertiesData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class LoginPage {

    WebDriver driver;
    @FindBy(id = "email")
    WebElement username;

    @FindBy(id = "pass")
    WebElement password;

    @FindBy(id="send2")
    WebElement submitBtn;

    @FindBy(xpath = "//h2[contains(.,'Already registered?')]")
    WebElement loginText;

    @FindBy(xpath = "//strong[contains(.,'TestJava Java')]")
    WebElement loginCheck;

    public LoginPage(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public void navigateToLoginPage() throws IOException {
        driver.navigate().to(PropertiesData.getData("loginnavigateurl"));
        if(loginText.getText().equalsIgnoreCase("Already registered?"))
        {
            System.out.println("You are on Login page");
        }
        else
        {
            System.out.println("Something went wrong");
        }
    }

    public void navigateToLoginPage(String url)
    {
        driver.navigate().to(url.trim());
        System.out.println("Opening the url"+url);
    }
    public void navigateToLoginPageUsingUrl(String url) throws IOException {
        driver.navigate().to(url);
//        if(loginText.getText().equalsIgnoreCase("Already registered?"))
//        {
//            System.out.println("You are on Login page");
//        }
//        else
//        {
//            System.out.println("Something went wrong");
//        }
    }

    public void enterUserName() throws IOException {

     /*  WebElement el=driver.findElement(By.id("email"));  @FindBy(id="") WebElement el
       el.sendKeys("xyz");*/

        username.sendKeys(PropertiesData.getData("loginusername"));

    }

    public void enterUserName(String usernamevalue)
    {
        username.clear();
        username.sendKeys(usernamevalue);
    }

    public void enterPassword() throws IOException {
        password.clear();
        password.sendKeys(PropertiesData.getData("loginpassword"));
    }

    public void enterPassword(String pass)
    {
        password.clear();
        password.sendKeys(pass);
    }
    public void enterCred(String usernamevalue,String passwordvalue)
    {
        username.sendKeys(usernamevalue);
        password.sendKeys(passwordvalue);
    }

    public void clickSumbit()
    {
        submitBtn.click();
    }


    public void validateLoginSuccessful()
    {
        if(loginCheck.getText().contains("TestJava Java"))
        {
            System.out.println("you have logged in successfully");
        }
        else {
            System.out.println("login issue");
        }
    }

}

