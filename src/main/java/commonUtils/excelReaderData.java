package commonUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

public class excelReaderData {

    static WebDriver driver;
    public static void main(String[] args) throws IOException {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\ghodk\\Downloads\\chromedriver_win32 (2)");

        driver=new ChromeDriver();
        driver.get(commonutils.PropertiesData.getData("login_url"));
        driver.manage().timeouts().getPageLoadTimeout();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        String filePath="C:\\Users\\ghodk\\IdeaProjects\\FinalFramework\\src\\main\\resources\\LoginCred.xlsx";
        readData(filePath,"UserPassDetails");
    }
    public static void readData(String excelfile,String sheetName) throws IOException {

        File file=new File(excelfile);
        FileInputStream fis=new FileInputStream(file);

        XSSFWorkbook workbook=new XSSFWorkbook(fis);
        XSSFSheet sheet=workbook.getSheet(sheetName);
        int rowsize= sheet.getLastRowNum();
        int colmsize=sheet.getRow(0).getLastCellNum();
        System.out.println("rowsize"+rowsize+"Col:"+colmsize);

        for (int i=1;i<=rowsize;i++)
        {
            driver.findElement(By.id("email")).clear();
            driver.findElement(By.id("email")).sendKeys(sheet.getRow(i).getCell(0).toString());
            driver.findElement(By.id("pass")).clear();
            driver.findElement(By.id("pass")).sendKeys(sheet.getRow(i).getCell(1).toString());


            System.out.println();
        }

    }
}
