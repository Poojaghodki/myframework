import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import stepDefination.Hooks;

import java.io.IOException;

public class TestPage {

    static LoginPage login;
    static  WebDriver driver;
    public static void main(String[] args) throws IOException {

//        System.setProperty("webdriver.chrome.driver","/Users/priyanka.a/Downloads/PageObjectFramework/src/main/resources/drivers/chromedriver");
//        WebDriver driver=new ChromeDriver();

        driver= Hooks.driver;
        login=new LoginPage(driver);
        login.navigateToLoginPage();
        login();
        login.validateLoginSuccessful();



    }

    public static void login() throws IOException {
        login.enterUserName();
        login.enterPassword();
        login.clickSumbit();

    }
}
