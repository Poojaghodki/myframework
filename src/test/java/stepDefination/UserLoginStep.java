
package stepDefination;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;

import java.io.IOException;

public class UserLoginStep {

    WebDriver driver;



    LoginPage login;

    @Given("user open browser and hit url")
    public void user_open_browser_and_hit_url() throws IOException {

//        System.setProperty("webdriver.chrome.driver","/Users/priyanka.a/Downloads/PageObjectFramework/src/main/resources/drivers/chromedriver");
//        WebDriver driver=new ChromeDriver();

        driver= stepDefination.Hooks.driver;
        login =new LoginPage(driver);
        login.navigateToLoginPage();

    }

    @When("^user enters (.+) and (.+)$")
    public void user_enters_and(String username, String password) throws Throwable {
        System.out.println("UserName:"+username+"Password:"+password);
        login.enterUserName(username);
        login.enterPassword(password);
    }
    @When("user enters username and password")
    public void user_enters_username_and_password() throws IOException {
        login.enterUserName();
        login.enterPassword();

    }

    /*@And("hit submit button")
    public void hit_submit_button() {
        login.clickSumbit();
    }

    @Then("validate user landing on welcome page")
    public void validate_user_landing_on_welcome_page() {
        login.validateLoginSuccessful();
    }

*/


}
