package stepDefination;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.logging.Logger;


public class Hooks {
    Logger log = Logger.getLogger("devpinoyLogger");
    // static  Logger log =Logger.getLogger(Hooks.class.getName());

    public static WebDriver driver;
    @Before(order = 0)
    public void testopenBrowser()
    {


        System.setProperty("webdriver.chrome.driver","C:\\Users\\ghodk\\Downloads\\chromedriver_win32 (2)\\chromedriver.exe");
        driver=new ChromeDriver();
        log.info("Browser open order 0");
    }

    @Before(order = 1)
    public void opentest()
    {
        System.out.println("b4 step order 1");
        log.info("Open Test");
    }

    @Before(order = 2)
    public void test()
    {
        System.out.println("test order 2");
    }
    @After(order = 0)
    public void closeBrowser()
    {
        //   driver.quit();
    }

    @After(order = 1)
    public void takeScreenshots(Scenario scenario) throws IOException {

        String screenshotName = scenario.getName().replaceAll(" ", "_");
        byte[] sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.attach(sourcePath, "image/png", scenario.getName().toString());
    }

    // before 0-1-2-10
    //after 10-9-8-0
}
