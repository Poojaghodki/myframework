package stepDefination;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;

import java.util.List;

public class datatabledef {

    WebDriver driver;
    LoginPage loginpage;
    @Given("valid user is opening Url")
    public void valid_user_is_opening_url(io.cucumber.datatable.DataTable dataTable) {

        driver= stepDefination.Hooks.driver;
        loginpage=new LoginPage(driver);
        List<List<String>> values=dataTable.asLists();
        System.out.println("Values using data table is :"+values.get(1).get(0).toString());
        loginpage.navigateToLoginPage(values.get(1).get(0).toString());
    }


    @When("valid user enter valid UserName and valid Password")
    public void valid_user_enter_valid_user_name_and_valid_password(io.cucumber.datatable.DataTable dataTable) {
        String userName,password;
        List<List<String>> usercred=dataTable.asLists();
        System.out.println(usercred.size());
        userName= usercred.get(0).get(0).toString();
        password=usercred.get(0).get(1).toString();
        System.out.println("User0:"+userName+"\nPass:"+password);
        loginpage.enterUserName(userName);
        loginpage.enterPassword(password);
        userName= usercred.get(1).get(0).toString();
        password=usercred.get(1).get(1).toString();
        System.out.println("User1:"+userName+"\nPass:"+password);
        loginpage.enterUserName(userName);
        loginpage.enterPassword(password);
        userName= usercred.get(2).get(0).toString();
        password=usercred.get(2).get(1).toString();
        System.out.println("User2:"+userName+"\nPass:"+password);
        loginpage.enterUserName(userName);
        loginpage.enterPassword(password);
        userName= usercred.get(3).get(0).toString();
        password=usercred.get(3).get(1).toString();
        System.out.println("User3:"+userName+"\nPass:"+password);
        loginpage.enterUserName(userName);
        loginpage.enterPassword(password);


    }

    @And("hit submit button")
    public void hit_submit_button() {
        loginpage.clickSumbit();
    }

    @Then("validate user landing on welcome page")
    public void validate_user_landing_on_welcome_page() {
        loginpage.validateLoginSuccessful();
    }



}
