package stepDefination;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;

import java.io.IOException;
import java.util.List;

public class dataTableStep {

    LoginPage loginPage;
    @Given("user open browser and hit Url")
    public void user_open_browser_and_hit_url(io.cucumber.datatable.DataTable dataTable) throws IOException {

        List<List<String>> url =dataTable.asLists();
        String urllink= url.get(1).get(0);
        System.out.println("Url:"+urllink.toString());


        loginPage=new LoginPage(stepDefination.Hooks.driver);
        loginPage.navigateToLoginPage();
    }

    @When("user enters Username and Password")
    public void user_enters_username_and_password(io.cucumber.datatable.DataTable dataTable) {

        List<List<String>> cred=dataTable.asLists();
        String username=cred.get(1).get(0);
        String password=cred.get(1).get(1);

        System.out.println(username+"\n password"+password);

        loginPage.enterCred(username,password);

    }
    @When("click on submit button")
    public void click_on_submit_button() {
        loginPage.clickSumbit();
    }

    @Then("validate user landing on welcome page successfully")
    public void validate_user_landing_on_welcome_page_successfully() {
        loginPage.validateLoginSuccessful();
    }
}
