package stepDefination;
import commonUtils.ExcelReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;
import stepDefination.Hooks;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


public class DataDrivenExcelStep {
    Logger log = Logger.getLogger("devpinoyLogger");
    String Excelbookpath="C:\\Users\\ghodk\\IdeaProjects\\FinalFramework\\src\\main\\resources\\LoginCred.xlsx";
    String username,Password,firstName,lastName;
    LoginPage loginPage;


    @Given("^user get details from (.+) for (.+)$")
    public void user_get_details_from_for(String sheetName, int testcaseid) throws Throwable {
        System.out.println(sheetName+"\n"+testcaseid);
        System.out.println("Excel sheet Name:"+Excelbookpath);
        System.out.println("SheetName:"+sheetName);
        log.info("SheetName using logger:"+sheetName);
        ExcelReader reader = new ExcelReader();
        List<Map<String,String>> testData =reader.getData(Excelbookpath,sheetName);
        System.out.println(testcaseid);
        username=testData.get(testcaseid).get("UserName");
        Password=testData.get(testcaseid).get("Password");

        /*firstName=testData.get(testcaseid).get("First");
        lastName=testData.get(testcaseid).get("LastN");*/
        System.out.println("UserName:"+username+"\n password:"+Password);
        loginPage=new LoginPage(Hooks.driver);
        loginPage.navigateToLoginPage();
    } @And("user hits the URL http:\\/\\/live.techpanda.org\\/index.php\\/customer\\/account\\/login\\/")
    public void userHitsTheURLHttpLiveTechpandaOrgIndexPhpCustomerAccountLogin() {
    }

    @Then("^verify login successfully$")
    public void verify_login_successfully() throws Throwable {

        loginPage.validateLoginSuccessful();


    }

    @When("^user enter cred details$")
    public void user_enter_cred_details() throws Throwable {

        loginPage.enterCred(username,Password);
    }


    @When("click submit button")
    public void click_submit_button() {
        loginPage.clickSumbit();
    }



    @And("user hits the URL")
    public void userHitsTheURL() {
    }
}
